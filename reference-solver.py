import random
import collections
from itertools import product

# big boggle alphabet of:
#    25 letters (sans Q)
#    6 digrams
# 31 possible values
# fits within a 5-bit encoding
# an encoded Big Boggle board can fit within a 128-bit value (125 bits)
encodings = {
    'A': 0b00000,
    'B': 0b00001,
    'C': 0b00010,
    'D': 0b00011,
    'E': 0b00100,
    'F': 0b00101,
    'G': 0b00110,
    'H': 0b00111,
    'I': 0b01000,
    'J': 0b01001,
    'K': 0b01010,
    'L': 0b01011,
    'M': 0b01100,
    'N': 0b01101,
    'O': 0b01110,
    'P': 0b01111,
    'R': 0b10000,
    'S': 0b10001,
    'T': 0b10010,
    'U': 0b10011,
    'V': 0b10100,
    'W': 0b10101,
    'X': 0b10110,
    'Y': 0b10111,
    'Z': 0b11000,
    'AN': 0b11001,
    'ER': 0b11010,
    'HE': 0b11011,
    'IN': 0b11100,
    'QU': 0b11101,
    'TH': 0b11110,
}

decodings = {
    0b00000: 'A',
    0b00001: 'B',
    0b00010: 'C',
    0b00011: 'D',
    0b00100: 'E',
    0b00101: 'F',
    0b00110: 'G',
    0b00111: 'H',
    0b01000: 'I',
    0b01001: 'J',
    0b01010: 'K',
    0b01011: 'L',
    0b01100: 'M',
    0b01101: 'N',
    0b01110: 'O',
    0b01111: 'P',
    0b10000: 'R',
    0b10001: 'S',
    0b10010: 'T',
    0b10011: 'U',
    0b10100: 'V',
    0b10101: 'W',
    0b10110: 'X',
    0b10111: 'Y',
    0b11000: 'Z',
    0b11001: 'AN',
    0b11010: 'ER',
    0b11011: 'HE',
    0b11100: 'IN',
    0b11101: 'QU',
    0b11110: 'TH',
}

blocks = [
    ["F", "S", "I", "R", "A", "A"],
    ["D", "N", "O", "L", "H", "R"],
    ["P", "C", "E", "I", "S", "T"],
    ["N", "N", "E", "M", "A", "G"],
    ["U", "E", "E", "G", "M", "A"],
    ["A", "A", "A", "F", "S", "R"],
    ["I", "Y", "P", "Y", "S", "R"],
    ["H", "N", "H", "W", "D", "O"],
    ["O", "T", "O", "O", "U", "T"],
    ["A", "E", "A", "E", "E", "E"],
    ["B", "X", "J", "K", "B", "Z"],
    ["S", "L", "I", "T", "E", "P"],
    ["S", "S", "S", "N", "E", "U"],
    ["I", "Y", "A", "F", "R", "S"],
    ["O", "R", "W", "G", "V", "R"],
    ["T", "I", "T", "I", "E", "I"],
    ["D", "L", "O", "H", "H", "R"],
    ["N", "N", "N", "E", "A", "D"],
    ["N", "E", "C", "C", "S", "T"],
    ["S", "I", "I", "E", "T", "L"],
    ["O", "U", "O", "T", "W", "N"],
    ["H", "D", "T", "N", "O", "D"],
    ["T", "T", "O", "T", "E", "M"],
    ["A", "E", "M", "E", "E", "E"],
    ["TH", "HE", "QU", "IN", "ER", "AN"],
]

encoded_blocks = [
    533029, 
    544586147, 
    622071887, 
    201724333, 
    12783763, 
    554860544, 
    555466472, 
    473603495, 
    624376398, 
    138543232, 
    806692545, 
    508109169, 
    642172465, 
    587367144, 
    558061070, 
    272910610, 
    544455011, 
    100808109, 
    621873293, 
    388112657, 
    458832494, 
    115787879, 
    407452242, 
    138555520, 
    867071870   
]

class Tree(object):
    def __init__(self):
        self.children = collections.defaultdict(Tree)
        self.complete = False # for full words

    def insert(self, word):
        if not word:
            self.complete = True
        else:
            self.children[word[0]].insert(word[1:])

    def check_word(self, word, prefix=False):
        if not word:
            return prefix or self.complete
        else:
            if word[0] in self.children:
                return self.children[word[0]].check_word(word[1:], prefix)
            else:
                return False

# RNG seed
random.seed(0x414141)

# open boggle dictionary, insert words into tree
tree = Tree()
with open('legal-boggle-dictionary-long.txt') as f:
    [tree.insert(word) for word in f.read().splitlines()]

def decode_board(encoded_board):
    values = [decodings[(encoded_board >> 5*i) & 0b11111] for i in range(25)]
    return [values[i:i+5] for i in range(0, 25, 5)]

def encoded_boardgen():
    letters = [(eb >> random.choice(range(6))*5 & 0b11111) for eb in encoded_blocks]
    random.shuffle(letters)
    return sum([(letters[i] << i*5) for i in range(25)])

def boardgen():
    letters = [random.choice(i) for i in blocks]
    random.shuffle(letters)
    return [letters[i:i+5] for i in range(0, 25, 5)]  

def neighbors(cell):
    for c in product(*(range(n-1, n+2) for n in cell)):
        if c != cell and all(0 <= n < 5 for n in c):
            yield c

def next_cells(cell, memory):
    nexts = []
    for n in neighbors(cell):
        if not n in memory:
            nexts.append(n)
    return nexts

def cell_paths(cell, board, protoword='', memory=[]):
    memory = memory + [cell]
    protoword = protoword + board[cell[0]][cell[1]]
    nexts = next_cells(cell, memory)
    if tree.check_word(protoword, prefix=False):
        found.add(protoword) 
    if len(nexts) == 0 or tree.check_word(protoword, prefix=True) == False:
        return
    for n in nexts:
        cell_paths(n, board, protoword, memory)

# solve a board
found = set()
encoded_board = encoded_boardgen()
board = decode_board(encoded_board)
for i in range(0,5):
    for j in range(0,5):
        cell_paths((i,j), board)

# print results
[print(row) for row in board]
print(sorted(found))
print(len(found))

